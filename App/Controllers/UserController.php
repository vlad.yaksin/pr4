<?php

namespace App\Controllers;

use GuzzleHttp\Client;
use App\Controllers\DbController;
use \DB;

class UserController 
{
    const DB_NAME = 'bd';
    const DB_USER_TABLE = 'users';

    

    public function addUser(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        if(empty($params['user']) || empty($params['password'])){
            echo "Fill the fields";
        }
        else{
            DB::insert(self::DB_USER_TABLE, $params);
        }
    }

    public function getUser(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        $where = (isset($params['user'])) ? ' WHERE user LIKE \'%' . $params['user'] . '%\'' : '';
        return DB::query("SELECT * FROM " . self::DB_USER_TABLE . " {$where}");
    }

    public function updateUser(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        if(empty($params['user']) || empty($params['password'])){
            echo "Fill the fields";
        }
        else{
            DB::update(self::DB_USER_TABLE, ['user' => $params['user'], 'password' => $params['password']], " id=%s", $params['id']);         
        }
    }

    public function deleteUser(array $params = [])
    {
        $dbController = new DbController();
        $dbController->createDbInstance(self::DB_NAME);
        DB::delete(self::DB_USER_TABLE, 'id=%s', $params['id']);
    }
}