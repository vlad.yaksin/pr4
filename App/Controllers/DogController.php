<?php

namespace App\Controllers;

use GuzzleHttp\Client;
use App\Controllers\DbController;
use \DB;

class DogController
{
    const USERS_API_URL = 'https://dog.ceo/api/breeds/image/random';
    public function getRandomDog($params = [])
    {
        $client = $this->getHttpClient();

        $responseContent = $client->request('GET', '', [
            'query' => $params])->getBody()->getContents();

        
        $data = json_decode($responseContent);

        return $data->message;
    }
    private function getHttpClient()
    {
        return new Client([
            'base_uri' => self::USERS_API_URL,
        ]);
    }
}