<?php
namespace App;

require __DIR__ . '/vendor/autoload.php';

use App\Controllers\UserController;

$user = new UserController();

$method = $_REQUEST['method'];

$json = file_get_contents("php://input");
$data = json_decode($json);

$methods = [
    'adduser' => 'App\userAddHandle',
    'getuser' => 'App\userHandle',
    'updateuser' => 'App\userUpdateHandle',
    'deleteuser' => 'App\userDeleteHandle',
];

if (empty($methods[$method]) || !function_exists($methods[$method])) {
    http_response_code(405);
    var_dump(function_exists('App\\'. $methods[$method]));
    echo 'Method ' . $methods[$method] . ' doesn\'t exitst';
    return;
}

echo $methods[$method]($data);

function userAddHandle($data) 
{
    $params = [
        'user' => $data->user,
        'password' => $data->password,
    ];
    $userController = new UserController();
    $userController->addUser($params);
} 

function userHandle($data) {
    $params = [
        'user' => $data->user
    ];
    $userController = new UserController();
    $user = $userController->getUser($params);
    if($user == []){
        echo "Nothing found";
    }
    else{
        header('Content-Type: application/json');
        echo json_encode($user);
    }
}

function userUpdateHandle($data) {
    $params = [
        'id' => $data->id,
        'user' => $data->user,
        'password' => $data->password,
    ];
    $userController = new UserController();
    $userController->updateUser($params);
}

function userDeleteHandle($data) {
    $params = [
        'id' => $data->id,
    ];
    $userController = new UserController();
    $userController->deleteUser($params);
}
